package uk.co.roteala.security;

import lombok.experimental.UtilityClass;
import uk.co.roteala.common.BasicModel;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.regex.Pattern;

@UtilityClass
public class Cryptography {
    private static final String MESSAGE_PREFIX = "\u0019Ethereum Signed Message:\n";

    private static final Pattern HEXADECIMAL_PATTERN = Pattern.compile("0x[a-fA-F0-9]{40}");

    private static byte[] getEthereumMessagePrefix(int messageLength) {
        return MESSAGE_PREFIX
                .concat(String.valueOf(messageLength))
                .getBytes(StandardCharsets.UTF_8);
    }

    private static byte[] getEthereumMessageHash(byte[] message) {
        byte[] prefix = getEthereumMessagePrefix(message.length);

        byte[] result = new byte[prefix.length + message.length];
        System.arraycopy(prefix, 0, result, 0, prefix.length);
        System.arraycopy(message, 0, result, prefix.length, message.length);

        return Hashing.sha3(result);
    }

    /**
     * Recover the public keys from a specific set of models
     * */
    public List<PublicKey> recoverPublicKeys(BasicModel model) {
        return null;
    }
}
