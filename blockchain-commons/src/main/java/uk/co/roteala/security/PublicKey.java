package uk.co.roteala.security;

public interface PublicKey extends Key {
    String getX();

    String getY();

    byte[] encodedX();

    byte[] encodedY();

    String getPubKeyHash();
}
