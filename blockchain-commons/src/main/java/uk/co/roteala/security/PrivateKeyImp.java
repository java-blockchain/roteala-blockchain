package uk.co.roteala.security;

import lombok.Builder;
import lombok.Setter;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveGenParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.math.ec.ECPoint;

import java.math.BigInteger;

public class PrivateKeyImp implements PrivateKey {

    @Setter
    private String secretHex;

    public PrivateKeyImp(String secretHex) {
        this.secretHex = secretHex;
    }

    @Override
    public KeyType getType() {
        return KeyType.PRIVATE;
    }

    @Override
    public BigInteger getSecret() {
        return new BigInteger(this.secretHex, 16);
    }

    @Override
    public String getHex() {
        return this.secretHex;
    }

    @Override
    public byte[] getEncoded() {
        return new BigInteger(this.secretHex, 16)
                .toByteArray();
    }

    @Override
    public String toAddress() {
        return this.getPublicKey()
                .toAddress();
    }

    @Override
    public PublicKey getPublicKey() {
        ECNamedCurveParameterSpec ecNamedCurveGenParameterSpec =
                ECNamedCurveTable.getParameterSpec("secp256k1");

        ECPoint ecPoint = ecNamedCurveGenParameterSpec.getG();

        ECPoint publicPoint = ecPoint
                .multiply(new BigInteger(this.secretHex, 16))
                .normalize();

        return new PublicKeyImp(publicPoint);
    }
}
