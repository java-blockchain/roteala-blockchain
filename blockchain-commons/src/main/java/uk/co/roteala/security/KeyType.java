package uk.co.roteala.security;

public enum KeyType {
    PRIVATE,
    PUBLIC;
}
