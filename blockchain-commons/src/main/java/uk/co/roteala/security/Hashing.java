package uk.co.roteala.security;

import lombok.experimental.UtilityClass;
import org.bouncycastle.jcajce.provider.digest.Keccak;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import uk.co.roteala.exceptions.HashingException;
import uk.co.roteala.exceptions.NoAlgorithmException;
import uk.co.roteala.exceptions.errorcodes.HashingErrorCode;
import uk.co.roteala.exceptions.errorcodes.NoAlgorithmErrorCodes;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

@UtilityClass
public class Hashing {
    public byte[] sha256Hash(byte[] input) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            MessageDigest sha256 = MessageDigest.getInstance("SHA-256");

            return sha256.digest(input);
        } catch (NoSuchAlgorithmException e) {
            throw new NoAlgorithmException(NoAlgorithmErrorCodes.NO_ALGORITHM);
        } catch (Exception e) {
            throw new HashingException(HashingErrorCode.FAILED_TO_HASH);
        }
    }

    public byte[] ripeMD160Hash(byte[] input) {
        try {
            Security.addProvider(new BouncyCastleProvider());
            MessageDigest ripemd160 = MessageDigest.getInstance("RIPEMD160");

            return ripemd160.digest(input);
        } catch (NoSuchAlgorithmException e) {
            throw new NoAlgorithmException(NoAlgorithmErrorCodes.NO_ALGORITHM);
        } catch (Exception e) {
            throw new HashingException(HashingErrorCode.FAILED_TO_HASH);
        }
    }

    public byte[] doubleSHA256(byte[] input) {
        return sha256Hash(sha256Hash(input));
    }

    public byte[] doubleSHA3(byte[] input) {
        return sha3(sha3(input));
    }

    public String bytesToHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();

        for (byte b : bytes) {
            String hex = String.format("%02x", b);
            hexString.append(hex);
        }

        return hexString.toString();
    }

    public byte[] keccak(byte[] bytes) {
        try {
            Keccak.DigestKeccak keccak = new Keccak.Digest256();
            keccak.update(bytes);

            return keccak.digest();
        } catch (Exception e) {
            throw new HashingException(HashingErrorCode.FAILED_TO_HASH);
        }
    }

    public static byte[] sha3(byte[] input, int offset, int length) {
        try {
            Keccak.DigestKeccak keccak = new Keccak.Digest256();
            keccak.update(input, offset, length);

            return keccak.digest();
        } catch (Exception e) {
            throw new HashingException(HashingErrorCode.FAILED_TO_HASH);
        }
    }

    public static byte[] sha3(byte[] input) {
        return sha3(input, 0, input.length);
    }

    public static byte[] hexStringToByteArray(String string) {
        String cleanInput = cleanHexPrefix(string);

        int len = cleanInput.length();

        if (len == 0) {
            return new byte[0];
        } else {
            byte[] data;
            byte startIdx;
            if (len % 2 != 0) {
                data = new byte[len / 2 + 1];
                data[0] = (byte)Character.digit(cleanInput.charAt(0), 16);
                startIdx = 1;
            } else {
                data = new byte[len / 2];
                startIdx = 0;
            }

            for(int i = startIdx; i < len; i += 2) {
                data[(i + 1) / 2] = (byte)((Character.digit(cleanInput.charAt(i), 16) << 4) + Character.digit(cleanInput.charAt(i + 1), 16));
            }

            return data;
        }
    }

    public static String cleanHexPrefix(String input) {
        return containsHexPrefix(input) ? input.substring(2) : input;
    }

    public static boolean containsHexPrefix(String input) {
        return !isEmpty(input) && input.length() > 1 && input.charAt(0) == '0' && input.charAt(1) == 'x';
    }

    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static String toHexString(byte[] input, int offset, int length, boolean withPrefix) {
        StringBuilder stringBuilder = new StringBuilder();

        if(withPrefix) {
            stringBuilder.append("0x");
        }

        for(int i = offset; i < offset + length; ++i) {
            stringBuilder.append(String.format("%02x", input[i] & 255));
        }

        return stringBuilder.toString();
    }

    public static String toHexString(byte[] input, boolean prefix) {
        return toHexString(input, 0, input.length, prefix);
    }
}
