package uk.co.roteala.security;

import java.math.BigInteger;

public interface PrivateKey extends Key {
    BigInteger getSecret();

    String getHex();

    byte[] getEncoded();

    String toAddress();

    PublicKey getPublicKey();
}
