package uk.co.roteala.security;

import lombok.Setter;
import org.bouncycastle.math.ec.ECPoint;

import java.nio.charset.StandardCharsets;

public class PublicKeyImp implements PublicKey{

    @Setter
    private final ECPoint ecPoint;

    public PublicKeyImp(ECPoint ecPoint) {
        this.ecPoint = ecPoint;
    }

    @Override
    public KeyType getType() {
        return KeyType.PUBLIC;
    }

    @Override
    public String toAddress() {
        return generateAddress();
    }

    @Override
    public String getX() {
        return this.ecPoint.getAffineXCoord()
                .toBigInteger()
                .toString(16);
    }

    @Override
    public String getY() {
        return this.ecPoint.getAffineYCoord()
                .toBigInteger()
                .toString(16);
    }

    @Override
    public byte[] encodedX() {
        return this.ecPoint.getAffineXCoord()
                .getEncoded();
    }

    @Override
    public byte[] encodedY() {
        return this.ecPoint.getAffineYCoord()
                .getEncoded();
    }

    @Override
    public String getPubKeyHash() {
        return Hashing.bytesToHexString(Hashing.doubleSHA3(toAddress()
                .getBytes(StandardCharsets.UTF_8)));
    }

    private String generateAddress() {
        final byte[] bX = this.encodedX();
        final byte[] bY = this.encodedY();

        byte[] newBxBy = new byte[bX.length + bY.length];

        System.arraycopy(bX, 0, newBxBy, 0, bX.length);
        System.arraycopy(bY, 0, newBxBy, bX.length, bY.length);

        byte[] keccak256hash = Hashing.keccak(newBxBy);

        byte[] addressBytes = new byte[20];
        System.arraycopy(keccak256hash, keccak256hash.length - 20, addressBytes, 0, 20);

        return Hashing.toHexString(addressBytes, true);
    }
}
