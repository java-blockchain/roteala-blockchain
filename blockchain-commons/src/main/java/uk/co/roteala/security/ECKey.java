package uk.co.roteala.security;

import lombok.Getter;
import org.bouncycastle.jcajce.provider.asymmetric.ec.BCECPrivateKey;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.math.ec.ECPoint;
import uk.co.roteala.exceptions.CryptographyException;
import uk.co.roteala.exceptions.errorcodes.CryptographyErrorCode;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.Security;

public class ECKey {

    @Getter
    private PrivateKey privateKey;

    @Getter
    private PublicKey publicKey;

    public ECKey(String secretHex) {
        this.privateKey = new PrivateKeyImp(secretHex);
        this.publicKey = this.privateKey.getPublicKey();
    }

    public ECKey(ECPoint ecPoint) {
        this.publicKey = new PublicKeyImp(ecPoint);
    }

    public ECKey() {
        generateRandomECKey();
    }

    /**
     * Generate a random private/public key pair
     * */
    private void generateRandomECKey() {
        Security.addProvider(new BouncyCastleProvider());
        KeyPair keys = null;

        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance("EC", "BC");

            ECNamedCurveParameterSpec ecSpec = ECNamedCurveTable.getParameterSpec("secp256k1");

            keyGen.initialize(ecSpec, new SecureRandom());

            keys =  keyGen.generateKeyPair();

            this.privateKey = new PrivateKeyImp(((BCECPrivateKey) keys.getPrivate())
                    .getD().toString(16));

            this.publicKey = privateKey.getPublicKey();
        } catch (Exception e) {
            throw new CryptographyException(CryptographyErrorCode.FAILED_TO_CREATE_PAIR);
        }
    }
}
