package uk.co.roteala.exceptions.errorcodes;

import uk.co.roteala.exceptions.ErrorCode;

public enum NoAlgorithmErrorCodes implements ErrorCode {
    NO_ALGORITHM("noalgorithm.001");

    NoAlgorithmErrorCodes(String key) {
        this.key = key;
    }

    final String key;

    @Override
    public String getKey() {
        return key;
    }
}
