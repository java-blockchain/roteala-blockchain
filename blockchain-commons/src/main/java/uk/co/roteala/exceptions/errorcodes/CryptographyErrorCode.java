package uk.co.roteala.exceptions.errorcodes;

import uk.co.roteala.exceptions.ErrorCode;

public enum CryptographyErrorCode implements ErrorCode {
    FAILED_TO_CREATE_PAIR("cryptography.001");

    CryptographyErrorCode(String key) {
        this.key = key;
    }

    final String key;

    @Override
    public String getKey() {
        return key;
    }
}
