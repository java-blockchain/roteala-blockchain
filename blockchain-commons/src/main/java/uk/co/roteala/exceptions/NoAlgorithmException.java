package uk.co.roteala.exceptions;

public class NoAlgorithmException extends ApplicationRuntimeException {
    public NoAlgorithmException(ErrorCode errorCode) {
        super(errorCode);
    }

    public NoAlgorithmException(ErrorCode errorCode, Object... args) {
        super(errorCode, args);
    }
}
