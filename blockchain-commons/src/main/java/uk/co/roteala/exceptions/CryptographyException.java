package uk.co.roteala.exceptions;

public class CryptographyException extends ApplicationRuntimeException {
    public CryptographyException(ErrorCode errorCode) {
        super(errorCode);
    }

    public CryptographyException(ErrorCode errorCode, Object... args) {
        super(errorCode, args);
    }
}
