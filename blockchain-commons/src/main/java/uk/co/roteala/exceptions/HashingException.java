package uk.co.roteala.exceptions;

public class HashingException extends ApplicationRuntimeException {
    public HashingException(ErrorCode errorCode) {
        super(errorCode);
    }

    public HashingException(ErrorCode errorCode, Object... args) {
        super(errorCode, args);
    }
}
