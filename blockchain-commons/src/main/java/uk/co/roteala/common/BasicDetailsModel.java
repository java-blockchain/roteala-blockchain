package uk.co.roteala.common;
/**
 * This stores uses data submited to the chain without any transaction involving
 * crypto coins, this model stores users IDs, or messages or anything that has a
 * template, processing this models does not needs any fees and are separated from
 * coins transactions
 * */
public abstract class BasicDetailsModel extends BasicModel {
}
