package uk.co.roteala.common;

public enum VirtualBalanceSign {
    PLUS,
    MINUS
}
