package uk.co.roteala.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import uk.co.roteala.exceptions.SerializationException;
import uk.co.roteala.exceptions.errorcodes.SerializationErrorCode;

import java.io.Serializable;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
public abstract class BasicModel implements Serializable {
    private final long serialVersionUID = 1L;

    @JsonIgnore
    protected ObjectMapper mapper = new ObjectMapper();

    @JsonIgnore
    public abstract byte[] getKey();

    @JsonIgnore
    protected String serialize() {
        try {
            return mapper.writeValueAsString(this);
        } catch (Exception e) {
            throw new SerializationException(SerializationErrorCode.SERIALIZATION_FAILED);
        }
    }
}
